package com.schue.stephan.catchandrelease.reverse;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReverseObserver {

    void articleChanged();

}
