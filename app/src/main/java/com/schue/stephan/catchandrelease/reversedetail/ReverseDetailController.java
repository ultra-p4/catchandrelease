package com.schue.stephan.catchandrelease.reversedetail;

import android.app.Activity;
import android.content.Intent;

import com.schue.stephan.catchandrelease.reverse.ReverseMainActivity;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReverseDetailController implements ReverseDetailActivityProvider {

    private ReverseDetailModel model;

    public ReverseDetailController(ReverseDetailModel model) {
        this.model = model;
    }


    @Override
    public void seeDetailTriggered(String articlename) {
        model.findArticle(articlename);
    }

    @Override
    public void releaseArticleTriggered(String username, String articlename) {
        model.doReverseArticle(username, articlename);
    }

    @Override
    public void goToMainActivity(Activity pastActivity, String username) {

        Intent intent = new Intent(pastActivity, ReverseMainActivity.class).putExtra("USER_NAME", username);
        pastActivity.startActivity(intent);

    }

}
