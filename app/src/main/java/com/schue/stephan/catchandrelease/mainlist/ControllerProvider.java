package com.schue.stephan.catchandrelease.mainlist;

import android.app.Activity;


public interface ControllerProvider {

    void articleListTriggered();

    void showDetails(Activity pastActivity, String username, String articlename, Class classFile);

    void articleListMyCatchTriggered(String username);

    void managerTriggered();

    void historyListTriggered();



}
