package com.schue.stephan.catchandrelease;

import android.util.JsonReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephan on 19.07.17.
 */

public class Article {


    private String articlename;
    private String description;
    private String price;
    private String releaser;
    private String articlecategory;

    public Article () {}


    public Article (String articlename, String description, String price, String releaser, String articlecategory) {
        this.articlename = articlename;
        this.description = description;
        this.price = price;
        this.releaser = releaser;
        this.articlecategory = articlecategory;
    }


    public String getArticlename() {
        return articlename;
    }

    public void setArticlename(String articlename) {
        this.articlename = articlename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getReleaser() {
        return releaser;
    }

    public void setReleaser(String releaser) {
        this.releaser = releaser;
    }

    public String getArticlecategory() {
        return articlecategory;
    }

    public void setArticlecategory(String articlecategory) {
        this.articlecategory = articlecategory;
    }







   public class JsonStream {

       private List<Article> articles = new ArrayList<>();



       public List<Article> ReadArticlesArray(JsonReader reader) throws IOException {
           reader.beginArray();
           while (reader.hasNext()) {
               articles.add(readArticle(reader));
           }
           reader.endArray();
           return articles;
       }



       public Article readArticle(JsonReader reader) throws IOException {
           String articlename = null;
           String description = null;
           String price = null;
           String releaser = null;
           String rental_offer = null;
           reader.beginObject();
           while (reader.hasNext()) {
               String name = reader.nextName();
               if (name.equals("articlename")) {
                   articlename = reader.nextString();
               } else if (name.equals("description")) {
                   description = reader.nextString();
               } else if (name.equals("price")) {
                   price = reader.nextString();
               } else if (name.equals("releaser")) {
                   releaser = reader.nextString();
               } else if (name.equals("articlecategory")) {
                   rental_offer = reader.nextString();
               } else {
                   reader.skipValue();
               }
           }
           //System.err.println(reader.toString());
           reader.endObject();
           return new Article(articlename, description, price, releaser, rental_offer);
       }

   }



}
