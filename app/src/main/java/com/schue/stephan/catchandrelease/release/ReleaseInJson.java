package com.schue.stephan.catchandrelease.release;

import android.os.StrictMode;
import android.util.JsonReader;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.ServerConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by stephan on 10.08.17.
 */

public class ReleaseInJson implements ReleaseDataProvider {

    private HttpURLConnection urlConnection;
    private URL url;
    private boolean statusResponse = false;



    @Override
    public boolean storeArticle(String username, String articlename, String articlecategory, String description, String price) {

        Article article = new Article(articlename, description, price, username, articlecategory);
        setOutputStream(article);

        return statusResponse;
    }





    //POST Connection


    private void setOutputStream(Article article) {


        InputStream in = null;


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);



        try {
            url = new URL(ServerConnection.getPROTOCOL() + ServerConnection.getIP() +
                    ":" + ServerConnection.getPORT() + "/RestApp/rest/mainlist/release");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(0);


            //JSON Output

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
           // String jsonString = "{\"name\":" + this.mName + "}";
            OutputStream os = urlConnection.getOutputStream();
            os.write(buildJsonObject(article).getBytes());
            os.flush();


            //JSON Input - Response
/*            in = new BufferedInputStream(urlConnection.getInputStream());
            readJsonStream(in);*/

            //Status Code
            int statusCode = urlConnection.getResponseCode();
            System.out.println(statusCode);
            System.out.println(statusCode);
            System.out.println(statusCode);

            if (statusCode == 201) {
                statusResponse = true;
            }
            else {
                statusResponse = false;
            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            urlConnection.disconnect();
        }

    }




    private String buildJsonObject(Article article) {

        String out = null;

        //Erzeuge JSON Objekt
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("articlename", article.getArticlename());
            jsonObject.accumulate("description", article.getDescription());
            jsonObject.accumulate("price", article.getPrice());
            jsonObject.accumulate("releaser", article.getReleaser());
            jsonObject.accumulate("articlecategory", article.getArticlecategory());
        } catch (JSONException e) {
            e.printStackTrace();
            System.err.println(e.toString());
        }


        //Konvertiere JSONObject zu JSON to String

        out = jsonObject.toString();
        System.out.println(jsonObject.toString());

        return out;
    }




    //Reader

    private String readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            System.out.println(in.toString());
            System.out.println(in.toString());
            System.out.println(in.toString());
            return in.toString();
        } finally {
            reader.close();
        }
    }



}
