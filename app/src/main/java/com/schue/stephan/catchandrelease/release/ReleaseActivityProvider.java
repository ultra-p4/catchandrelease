package com.schue.stephan.catchandrelease.release;

import android.app.Activity;

import java.sql.SQLException;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReleaseActivityProvider {

    public void releaseArticleTriggered(String username, String articlename,
                                        String articlecategory, String description, String price)
            throws SQLException;


    public void goToMainActivity(Activity pastActivity, String username);


}
