package com.schue.stephan.catchandrelease.reverse;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;
import com.schue.stephan.catchandrelease.activity.MainActivityAbout;
import com.schue.stephan.catchandrelease.login.LoginActivity;
import com.schue.stephan.catchandrelease.activity.MainActivity;
import com.schue.stephan.catchandrelease.activity.ManagerAcitvity;
import com.schue.stephan.catchandrelease.activity.MyCatchActivity;
import com.schue.stephan.catchandrelease.profil.UserProfilActivity;
import com.schue.stephan.catchandrelease.release.MainActivityRelease;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReverseMainActivity extends AppCompatActivity
        implements ReverseObserver, NavigationView.OnNavigationItemSelectedListener {


    List<String> list = new ArrayList<>();
    ListView articleListView;
    private ReverseModel articleModel = new ReverseModel(this);
    private ReverseMainController controller = new ReverseMainController(articleModel);
    User user = new User("user");
    TextView headerUsername;




    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        user.setUsername(getIntent().getStringExtra("USER_NAME"));

        //Welcher Benutzer ist eingeloggt?  --> ok!
        //username = getIntent().getStringExtra("USER_NAME");
        System.out.println(user);
        System.out.println(user);
        System.out.println(user);
        System.out.println(user);
        System.out.println(user);


        //Menu und ListView in Activity laden, Activity anzeigen


        setContentView(R.layout.activity_main);

        //Adapter aufrufen

        articleListView = (ListView) findViewById(R.id.articleListView);
        articleListView.setAdapter(new ReverseActivityAdapter(user, this, controller, this, R.layout.content_main, articleModel.fetchArticles(user.getUsername())));




        //Menubar und Navigation laden

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        //Navigation zusammenstellen
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



     /*   AlertDialog.Builder builder = new AlertDialog.Builder(History.this);
        builder.setTitle("Hallo Welt");
        builder.setMessage(list.get().collect(Collectors.joining(", ")));
        builder.show();*/





     /*   FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        //Benutzername in Header einfügen
        headerUsername = (TextView) findViewById(R.id.navHeaderUserTextView);
        headerUsername.setText(user.getUsername());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {

            Intent intent = new Intent(getBaseContext(), UserProfilActivity.class).putExtra("USER_NAME", user.getUsername());


            startActivity(intent);

        } else if (id == R.id.nav_torelease) {

            Intent intent = new Intent(getBaseContext(),MainActivityRelease.class).putExtra("USER_NAME", user.getUsername());


            startActivity(intent);

        } else if (id == R.id.nav_home) {

            Intent intent = new Intent(getBaseContext(),MainActivity.class).putExtra("USER_NAME", user.getUsername());


            startActivity(intent);

        } else if (id == R.id.nav_mycatches) {

            Intent intent = new Intent(getBaseContext(),MyCatchActivity.class).putExtra("USER_NAME", user.getUsername());


            startActivity(intent);


        } else if (id == R.id.nav_reverse) {

            Intent intent = new Intent(getBaseContext(),ReverseMainActivity.class).putExtra("USER_NAME", user.getUsername());


            startActivity(intent);

        } else if (id == R.id.nav_manager) {

            Intent intent = new Intent(getBaseContext(),ManagerAcitvity.class).putExtra("USER_NAME", user.getUsername());


            startActivity(intent);


        } else if (id == R.id.nav_about) {

            Intent intent = new Intent(getBaseContext(),MainActivityAbout.class);


            startActivity(intent);

        } else if (id == R.id.nav_logout) {

            Intent intent = new Intent(getBaseContext(),LoginActivity.class);

            //Back Stack History zurücksetzen
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;





    }





    //Methoden

    @Override
    public void articleChanged() {
        System.out.println(articleModel.getCurrentArticle().getArticlename() + "mvc funktioniert");
    }
}
