package com.schue.stephan.catchandrelease;

import android.util.JsonReader;

import java.io.IOException;

public class Statistics {

    private int amountCatches;
    private int amountReleases;
    private int amountUsers;
    private int amountArticles;

    public Statistics () {}


    public Statistics (int amountCatches, int amountReleases, int amountUsers, int amountArticles) {
        this.amountCatches = amountCatches;
        this.amountReleases = amountReleases;
        this.amountUsers = amountUsers;
        this.amountArticles = amountArticles;
    }


    public int getAmountCatches() {
        return amountCatches;
    }

    public void setAmountCatches(int amountCatches) {
        this.amountCatches = amountCatches;
    }

    public int getAmountReleases() {
        return amountReleases;
    }

    public void setAmountReleases(int amountReleases) {
        this.amountReleases = amountReleases;
    }

    public int getAmountUsers() {
        return amountUsers;
    }

    public void setAmountUsers(int amountUsers) {
        this.amountUsers = amountUsers;
    }

    public int getAmountArticles() {
        return amountArticles;
    }

    public void setAmountArticles(int amountArticles) {
        this.amountArticles = amountArticles;
    }



    public class JsonStream {


        public Statistics readStatistics(JsonReader reader) throws IOException {
            Statistics statistics = new Statistics(0,0,0,0);
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("amountCatches")) {
                    statistics.setAmountCatches(reader.nextInt());
                } else if (name.equals("amountReleases")) {
                    statistics.setAmountReleases(reader.nextInt());
                } else if (name.equals("amountUsers")) {
                    statistics.setAmountUsers(reader.nextInt());
                } else if (name.equals("amountArticles")) {
                    statistics.setAmountArticles(reader.nextInt());
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            return statistics;
        }




    }




}
