package com.schue.stephan.catchandrelease.newkonto;

import com.schue.stephan.catchandrelease.HttpInputOutputStream;
import com.schue.stephan.catchandrelease.User;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by stephan on 14.08.17.
 */

public class KontoInJson implements KontoDataProvider {

    private HttpURLConnection urlConnection;
    private URL url;
    private boolean statusResponse;


    private static String INPUT_LINK_USER_REGISTER = "/RestApp/rest/mainlist/user/register";


    @Override
    public boolean storeUser(User user) {
        writeJsonStream(INPUT_LINK_USER_REGISTER, user);
        return statusResponse;
    }




    private void writeJsonStream(String link, User user) {
        HttpInputOutputStream httpInputOutputStream = new HttpInputOutputStream();
        User.JsonStream jsonStream = (new User()).new JsonStream();
        try {
            statusResponse = httpInputOutputStream.setOutputStream(link, jsonStream.buildJsonObject(user));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }






}






