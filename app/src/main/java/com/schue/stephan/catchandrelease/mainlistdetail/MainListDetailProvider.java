package com.schue.stephan.catchandrelease.mainlistdetail;

import android.app.Activity;


public interface MainListDetailProvider {

    void seeDetailTriggered(String articlename);

    void catchArticleTriggered(String articlename, String username);

    void goToMainActivity(Activity pastActivity, String username, Class classFile);

    void releaseCatchedArticle(String username, String articlename);


}
