package com.schue.stephan.catchandrelease.reversedetail;

import android.app.Activity;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReverseDetailActivityProvider {

    public void seeDetailTriggered(String articlename);

    public void releaseArticleTriggered(String articlename, String username);

    public void goToMainActivity(Activity pastActivity, String username);


}
