package com.schue.stephan.catchandrelease.release;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class MainActivityRelease extends AppCompatActivity
        implements ReleaseObserver {

    //private ReleaseModel model = new ReleaseModel(this);
    private ReleaseModel model = new ReleaseModel(this);
    private ReleaseController controller = new ReleaseController(model);
    User user = new User("user");

    private EditText editTextToReleaseTitel;
    private EditText editTextToReleaseKategorie;
    private EditText editTextToReleaseBeschreibung;
    private EditText editTextToReleasePreis;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        user.setUsername(getIntent().getStringExtra("USER_NAME"));

        setContentView(R.layout.activity_to_release);

        editTextToReleaseTitel = (EditText) findViewById(R.id.editTextToReleaseTitel);
        editTextToReleaseKategorie = (EditText) findViewById(R.id.editTextToReleaseKategorie);
        editTextToReleaseBeschreibung = (EditText) findViewById(R.id.editTextToReleaseDescription);
        editTextToReleasePreis = (EditText) findViewById(R.id.editTextToReleasePreis);

        Button buttonRelease = (Button) findViewById(R.id.buttonRelease);




        //Listener

        buttonRelease.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                //Überprüfung der Eingaben in den Textfeldern

                if(TextUtils.isEmpty(editTextToReleaseTitel.getText().toString())) {
                    editTextToReleaseTitel.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(editTextToReleaseKategorie.getText().toString())) {
                    editTextToReleaseKategorie.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(editTextToReleaseBeschreibung.getText().toString())) {
                    editTextToReleaseBeschreibung.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(editTextToReleasePreis.getText().toString())) {
                    editTextToReleasePreis.setError("Attribut cannot be empty!");
                }
                else {

                    String articlename = editTextToReleaseTitel.getText().toString();
                    String articlecategory = editTextToReleaseKategorie.getText().toString();
                    String description = editTextToReleaseBeschreibung.getText().toString();
                    String price = editTextToReleasePreis.getText().toString();

                    controller.releaseArticleTriggered(user.getUsername(), articlename,
                            articlecategory, description, price);

                }
            }

        });

    }






    //Methoden

    @Override
    public void storedArticle(String articlename) {
        Toast.makeText(this, "Artikel " + articlename + " wurde in die Datenbank hinzugefügt!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername());
    }

    @Override
    public void notStoredArticle(String articlename) {
        Toast.makeText(this, "Artikel " + articlename + " konnte nicht erfasst werden!", Toast.LENGTH_LONG).show();
    }
}
