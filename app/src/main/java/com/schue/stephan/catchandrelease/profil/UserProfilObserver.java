package com.schue.stephan.catchandrelease.profil;

import com.schue.stephan.catchandrelease.User;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface UserProfilObserver {

    public void userDataProvided(User user);

    public void userDataNotProvided(String errorMessage);
}
