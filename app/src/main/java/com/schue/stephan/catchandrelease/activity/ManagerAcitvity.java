package com.schue.stephan.catchandrelease.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.Statistics;
import com.schue.stephan.catchandrelease.User;
import com.schue.stephan.catchandrelease.mainlist.Controller;
import com.schue.stephan.catchandrelease.mainlist.Observer;
import com.schue.stephan.catchandrelease.mainlist.Model;

import java.util.List;


public class ManagerAcitvity extends AppCompatActivity implements Observer {


    User user = new User("user");

    private EditText catchesText;
    private EditText releasesText;
    private EditText articlesText;
    private EditText usersText;
    Button showHistoryButton;
    //private ManagerModel model = new ManagerModel(this);
    //private ManagerController controller = new ManagerController(model);
    private Model articleModel = new Model(this);
    private Controller controller = new Controller(articleModel);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        user.setUsername(getIntent().getStringExtra("USER_NAME"));
        catchesText = (EditText) findViewById(R.id.catchEditText);
        releasesText = (EditText) findViewById(R.id.releasesEditText);
        articlesText = (EditText) findViewById(R.id.articlesEditText);
        usersText = (EditText) findViewById(R.id.usersEditText);
        showHistoryButton = (Button) findViewById(R.id.showHistoryButton);
        if (user.getUsername().equals("Marcel")) {
            showHistoryButton.setClickable(true);
        } else {
            showHistoryButton.setClickable(false);
            showHistoryButton.setEnabled(false);
            showHistoryButton.setBackgroundColor(0xFFFFFFFF);
        }
        catchesText.setFocusable(false);
        releasesText.setFocusable(false);
        articlesText.setFocusable(false);
        usersText.setFocusable(false
        );
        showHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerAcitvity.this, History.class);
                intent.putExtra("USER_NAME", user.getUsername());
                startActivity(intent);
            }
        });
        try {
            controller.managerTriggered();

        } catch   (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void articleChanged() {

    }



    @Override
    public void listResultValid(List<Article> articles) {

    }



    @Override
    public void listResultInvalid(String errorMessage) {

    }



    @Override
    public void listResultEmpty() {

    }



    @Override
    public void objectResultValid(Object object) {
        Statistics statistics = (Statistics) object;
        int catchValue = statistics.getAmountCatches();
        int releaseValue = statistics.getAmountReleases();
        int usersValue = statistics.getAmountUsers();
        int articlesValue = statistics.getAmountArticles();
        releasesText.setText(String.valueOf(releaseValue));
        catchesText.setText(String.valueOf(catchValue));
        usersText.setText(String.valueOf(usersValue));
        articlesText.setText(String.valueOf(articlesValue));
    }



    @Override
    public void objectResultInvalid(String errMessage) {
        releasesText.setText("");
        catchesText.setText("");
        usersText.setText("");
        articlesText.setText("");
        Toast.makeText(this, "There are no Attributes!!!", Toast.LENGTH_LONG).show();
    }



    @Override
    public void objectResultEmpty() {

    }


}


