package com.schue.stephan.catchandrelease.newkonto;

import android.app.Activity;
import android.content.Intent;

import com.schue.stephan.catchandrelease.User;
import com.schue.stephan.catchandrelease.login.LoginActivity;

import static com.schue.stephan.catchandrelease.R.string.username;

/**
 * Created by systemadmin on 30.07.2017.
 */

public class KontoController implements KontoActivityProvider {


    private KontoModel model;

    public KontoController(KontoModel model) {
        this.model = model;
    }



    @Override
    public void registrationTriggered(User user) {
        model.saveUser(user);
    }

    @Override
    public void goToMainActivity(Activity pastActivity, String newUsername) {
        Intent intent = new Intent(pastActivity, LoginActivity.class).putExtra("USER_NAME", username);
        pastActivity.startActivity(intent);
    }


}
