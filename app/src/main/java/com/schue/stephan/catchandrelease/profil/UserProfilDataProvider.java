package com.schue.stephan.catchandrelease.profil;

import com.schue.stephan.catchandrelease.User;

import java.io.IOException;


public interface UserProfilDataProvider {
    User findUser(String username) throws IOException;
}
