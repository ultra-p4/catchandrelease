package com.schue.stephan.catchandrelease.mainlistdetail;

import com.schue.stephan.catchandrelease.Article;




public class MainListDetailModel {

    private MainListDetailObserver observer;
    private MainListDetailInDataProvider database = new MainListDetailJson();


    public MainListDetailModel(MainListDetailObserver observer) {
        this.observer = observer;
    }

    public void findArticle(String articlename) {

        Article article = database.findArticle(articlename);

        if (article != null) {
            observer.productSelected(article.getArticlename(), article.getDescription(),
                    article.getArticlecategory(), article.getReleaser(), article.getPrice(), article.getPrice());
        } else {
            observer.notCatched(articlename);
        }
    }

    public void catchArticle(String username, String articlename) {
            database.rentalArticle(username, articlename);
            observer.catchesSuccessfully(articlename);
    }


    public void releaseCatchedArticle(String username, String articlename) {
        database.releaseCatchedArticle(username, articlename);
        observer.releasedSuccessfully(articlename);
    }

}
