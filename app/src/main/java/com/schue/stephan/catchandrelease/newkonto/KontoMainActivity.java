package com.schue.stephan.catchandrelease.newkonto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;

/**
 * Created by systemadmin on 30.07.2017.
 */

public class KontoMainActivity extends AppCompatActivity implements KontoObserver {

    private KontoModel model = new KontoModel(this);
    private KontoController controller = new KontoController(model);


    private EditText newUser;
    private EditText newPassword;
    private EditText newFirstname;
    private EditText newLastname;
    private EditText newEmail;
    private EditText newStreet;
    private EditText newPlz;
    private EditText newCity;


    private Button buttonRegistrieren;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_konto);

        newUser = (EditText) findViewById(R.id.editTextUsername);
        newPassword = (EditText) findViewById(R.id.editTextPassword);
        newFirstname = (EditText) findViewById(R.id.editTextFirstname);
        newLastname = (EditText) findViewById(R.id.editTextLastname);
        newEmail = (EditText) findViewById(R.id.editTextEmail);
        newStreet = (EditText) findViewById(R.id.editTextStreet);
        newPlz = (EditText) findViewById(R.id.editTextPlz);
        newCity = (EditText) findViewById(R.id.editTextCity);


        Button buttonRegistrieren = (Button) findViewById(R.id.buttonRegisterUser);

        buttonRegistrieren.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //Überprüfung der Eingaben in den Textfeldern

                if(TextUtils.isEmpty(newUser.getText().toString())) {
                    newUser.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(newPassword.getText().toString())) {
                    newPassword.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(newFirstname.getText().toString())) {
                    newFirstname.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(newLastname.getText().toString())) {
                    newLastname.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(newEmail.getText().toString())) {
                    newEmail.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(newStreet.getText().toString())) {
                    newStreet.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(newPlz.getText().toString())) {
                    newPlz.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(newCity.getText().toString())) {
                    newCity.setError("Attribut cannot be empty!");
                }
                else {

                    //Userdaten weitergeben

                    User user = new User(newUser.getText().toString(), newPassword.getText().toString(),
                            newFirstname.getText().toString(), newLastname.getText().toString(),
                            newEmail.getText().toString(), "void",
                            newStreet.getText().toString(), newPlz.getText().toString(),
                            newCity.getText().toString());

                    controller.registrationTriggered(user);
                }

            }
        });
    }

    @Override
    public void storedUser(String newUsername) {
        Toast.makeText(this, "User " + newUsername + " wurde in der Datenbank erstellt!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, newUsername);
    }



    @Override
    public void notStoredUser(String newUsername) {
            Toast.makeText(this, "User " + newUsername + " existiert bereits!", Toast.LENGTH_LONG).show();

    }
}


