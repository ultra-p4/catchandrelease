package com.schue.stephan.catchandrelease.reverse;

import android.app.Activity;
import android.content.Intent;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.activity.MainActivity;
import com.schue.stephan.catchandrelease.reversedetail.ReverseDetailMainActivity;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReverseMainController implements ReverseMainActivityProvider {

    private ReverseModel model;


    public ReverseMainController(ReverseModel model) {
        this.model = model;
    }

    @Override
    public void articleListTriggered(String username) {
        Article article = model.findArticle(username);
        model.setCurrentArticle(article);
    }

    @Override
    public void showDetails(Activity pastActivity, String username, String articlename) {
        Article article = model.findArticle(articlename);
        Intent intent = new Intent(pastActivity, ReverseDetailMainActivity.class);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("ARTICLE_NAME", articlename);
        pastActivity.startActivity(intent);
    }


    @Override
    public void goToMainActivity(Activity pastActivity, String username) {

        Intent intent = new Intent(pastActivity, MainActivity.class).putExtra("USER_NAME", username);
        pastActivity.startActivity(intent);

    }
}
