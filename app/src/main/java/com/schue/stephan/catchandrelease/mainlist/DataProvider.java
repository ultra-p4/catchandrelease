package com.schue.stephan.catchandrelease.mainlist;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.Statistics;

import java.io.IOException;
import java.util.List;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface DataProvider {

     List<Article> getArticles() throws IOException;

     List<Article> getMyCatchArticles(String username) throws IOException;

     Article findArticle(String articlename);

     Statistics getStatistics();

     List<Article> getHistory();

}
