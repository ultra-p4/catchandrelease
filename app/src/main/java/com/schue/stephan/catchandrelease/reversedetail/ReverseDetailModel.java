package com.schue.stephan.catchandrelease.reversedetail;

import com.schue.stephan.catchandrelease.Article;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReverseDetailModel {

    private ReverseDetailObserver observer;
    private ReverseDetailDataProvider database = new ReverseDetailJson();
    private String FeedbackString = "";





    ReverseDetailModel(ReverseDetailObserver observer) {
        this.observer = observer;
    }




    public void findArticle(String articlename) {

        Article article = database.findArticle(articlename);

        if (article != null) {
            observer.productSelected(article.getArticlename(), article.getDescription(),
                    article.getArticlecategory(), article.getReleaser(), article.getPrice(), article.getPrice());
        } else {
            observer.productNotExisting(articlename);
        }

    }




    public void doReverseArticle(String username, String articlename) {

            boolean storeState = database.rentalReverseArticle(username, articlename);

        if (storeState == true) {
            observer.reversedSuccessfully(articlename);
        } else {
            observer.notReversedSuccessfully(articlename);
        }
    }

}
