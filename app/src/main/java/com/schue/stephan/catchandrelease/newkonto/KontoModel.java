package com.schue.stephan.catchandrelease.newkonto;

import com.schue.stephan.catchandrelease.User;

/**
 * Created by systemadmin on 30.07.2017.
 */

public class KontoModel {

    private KontoObserver kontoObserver;
    private KontoDataProvider database = new KontoInJson();



    public KontoModel(KontoObserver kontoObserver) {
        this.kontoObserver = kontoObserver;
    }




    public void saveUser(User user){

        boolean storeState = database.storeUser(user);
        if (storeState == true) {
            kontoObserver.storedUser(user.getUsername());
        } else {
            kontoObserver.notStoredUser(user.getUsername());
        }
    }



}
