package com.schue.stephan.catchandrelease.newkonto;

import android.app.Activity;

import com.schue.stephan.catchandrelease.User;

import java.sql.SQLException;

/**
 * Created by systemadmin on 30.07.2017.
 */

public interface KontoActivityProvider {


    public void registrationTriggered(User user)

            throws SQLException;


    public void goToMainActivity(Activity pastActivity, String username);
}
