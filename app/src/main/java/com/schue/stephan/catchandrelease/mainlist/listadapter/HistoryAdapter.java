package com.schue.stephan.catchandrelease.mainlist.listadapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;
import com.schue.stephan.catchandrelease.activity.MainActivityAbout;
import com.schue.stephan.catchandrelease.mainlist.Controller;

import java.util.List;

/**
 * Created by systemadmin on 11.06.2017.
 */

public class HistoryAdapter extends ArrayAdapter {

    private final int layoutResource;
    private final LayoutInflater layoutInflater;
    private final Context context;
    private final Controller controller;
    private List<Article> articles;
    private User user;
    private Activity activity;
    //private final HistoryController controller;


    public HistoryAdapter(User user, Activity activity, Controller controller, @NonNull Context context, @LayoutRes int resource, @NonNull List<Article> articles) {
        super(context, resource, articles);
        this.context = context;
        this.user = user;
        this.controller = controller;
        this.layoutResource = resource;
        this.layoutInflater = LayoutInflater.from(context);
        this.articles = articles;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);


        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.mainlist_record, parent, false);
            convertView.setClickable(true);
            convertView.setFocusable(true);
        }

            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
            Button prodButton = (Button)  convertView.findViewById(R.id.articleButton);
            prodButton.setClickable(false);
            prodButton.setFocusable(false);
            prodButton.setEnabled(false);

 /*           imageButton.setFocusable(false);
            imageButton.setClickable(false);
            imageButton.setEnabled(false);*/


            //Listenelemente mit Attributen befüllen
            TextView articlename = (TextView) convertView.findViewById(R.id.articleNameText);
            TextView articlecategory = (TextView) convertView.findViewById(R.id.articleCategoryText);
            TextView releaser = (TextView) convertView.findViewById(R.id.releaserText);
            TextView price = (TextView) convertView.findViewById(R.id.priceText);
            Article article = articles.get(position);
            articlename.setText(article.getArticlename());
            articlecategory.setText(article.getDescription());
            releaser.setText(article.getPrice());
            if (article.getReleaser().equals("Release")) {
                releaser.setTextColor(0xFFDE8160);
                prodButton.setBackgroundColor(0xFFDE8160);
                prodButton.setTextColor(0xFFFFFFFF);
            } else if (article.getReleaser().equals("Reverse")) {
                releaser.setTextColor(0xFF4F5B66);
                prodButton.setBackgroundColor(0xFF4F5B66);
                prodButton.setTextColor(0xFFFFFFFF);
            }
            price.setText("");
            prodButton.setText(article.getReleaser());


            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    System.out.println("hallo");
                }

            });

    /*        imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*/

            prodButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    controller.showDetails(activity, user.getUsername(), articlename.getText().toString(), MainActivityAbout.class);
                    System.out.println(user.getUsername());
                }
            });



        return convertView;
    }

}
