package com.schue.stephan.catchandrelease.reversedetail;

import com.schue.stephan.catchandrelease.Article;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReverseDetailDataProvider {

    public Article findArticle(String articlename);

    public String isReversePossible(String username, String articlename);

    public boolean rentalReverseArticle(String username, String articlename);

}
