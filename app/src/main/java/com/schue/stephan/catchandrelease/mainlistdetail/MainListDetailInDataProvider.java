package com.schue.stephan.catchandrelease.mainlistdetail;

import com.schue.stephan.catchandrelease.Article;



public interface MainListDetailInDataProvider {

    Article findArticle(String articlename);

    void rentalArticle(String username, String articlename);

    void releaseCatchedArticle(String username, String articlename);

}
