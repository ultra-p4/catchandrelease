package com.schue.stephan.catchandrelease.login;

import android.util.JsonReader;

import com.schue.stephan.catchandrelease.HttpInputOutputStream;
import com.schue.stephan.catchandrelease.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

/**
 * Created by stephan on 08.08.17.
 */

public class LoginJson implements LoginDataProvider {

    private static String INPUT_LINK_FIND_USER = "/RestApp/rest/mainlist/search/";
    private User user = new User();



    @Override
    public User findUser(String username, String password) {
        try {
        readJsonStream (INPUT_LINK_FIND_USER + URLEncoder.encode(username,"UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }




   public void readJsonStream (String link) throws IOException {
        HttpInputOutputStream httpInputOutputStream = new HttpInputOutputStream();
        StringBuilder result = new StringBuilder();
        InputStream inputStream = httpInputOutputStream.getInputStream (link);
       try {
        new JsonReader(new InputStreamReader(inputStream));
       BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
           String line;
           while ((line = reader.readLine()) != null) {
               result.append(line);
           }
           reader.close();
       } catch (Exception e) {
           e.printStackTrace();
           System.err.println("JSON Error: " + e.toString());
       }
       httpInputOutputStream.closeConnection();
       user.setUsername(result.toString());
   }





}
