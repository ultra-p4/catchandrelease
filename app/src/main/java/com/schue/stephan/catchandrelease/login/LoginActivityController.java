package com.schue.stephan.catchandrelease.login;

import android.app.Activity;
import android.content.Intent;
import com.schue.stephan.catchandrelease.activity.MainActivity;



public class LoginActivityController implements LoginActivityProvider {

    private LoginModel model;



    LoginActivityController(LoginModel model) {
        this.model = model;
    }




    @Override
    public void loginTriggerd(String username, String password) {
        model.authenticateUser(username, password);
    }



    @Override
    public void goToProductActivity(Activity pastActivity, String username) {
        Intent intent = new Intent(pastActivity, MainActivity.class).putExtra("USER_NAME", username);
        pastActivity.startActivity(intent);

    }

}
