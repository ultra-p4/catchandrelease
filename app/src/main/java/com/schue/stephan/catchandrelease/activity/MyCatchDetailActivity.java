package com.schue.stephan.catchandrelease.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;
import com.schue.stephan.catchandrelease.mainlistdetail.MainListDetailController;
import com.schue.stephan.catchandrelease.mainlistdetail.MainListDetailModel;
import com.schue.stephan.catchandrelease.mainlistdetail.MainListDetailObserver;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class MyCatchDetailActivity extends AppCompatActivity implements MainListDetailObserver {

   // private MyCatchDetailModel articleModel = new MyCatchDetailModel(this);
   // private MyCatchDetailController controller = new MyCatchDetailController(articleModel);
    private MainListDetailModel articleModel = new MainListDetailModel(this);
    private MainListDetailController controller = new MainListDetailController(articleModel);
    TextView titelText;
    TextView beschreibungText;
    TextView categoryText;
    TextView releaserText;
    TextView priceText;
    Button catchButton;
    User user = new User("user");
    String articlename;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mainlistdetail);


        titelText = (TextView) findViewById(R.id.titelText);
        beschreibungText = (TextView) findViewById(R.id.beschreibungText);
        categoryText = (TextView) findViewById(R.id.categoryText);
        releaserText = (TextView) findViewById(R.id.releaserText);
        priceText = (TextView) findViewById(R.id.preisText);
        catchButton = (Button) findViewById(R.id.catchArticle);

        user.setUsername(getIntent().getStringExtra("USER_NAME"));
        articlename = (getIntent().getStringExtra("ARTICLE_NAME"));

        controller.seeDetailTriggered(articlename);

        catchButton.setText("RELEASE !");
        catchButton.setBackgroundColor(0xFFDE8160);  //#de8160 TerraCotta-Red (RGB: 222, 129, 96)
        beschreibungText.setMovementMethod(new ScrollingMovementMethod());
        beschreibungText.setFocusable(true);
        beschreibungText.setFocusableInTouchMode(true);


        catchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.releaseCatchedArticle(user.getUsername(), articlename);
            }
        });


    }

    @Override
    public void productSelected(String articlename, String label, String description, String releaser, String articlecategory, String price) {
        titelText.setText(articlename);
        beschreibungText.setText(label);
        categoryText.setText(articlecategory);
        releaserText.setText(releaser);
        priceText.setText(description);
    }

    @Override
    public void productNotExisting(String articlename) {
        Toast.makeText(this, "Article " + articlename + " Not Exists!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername(), MyCatchActivity.class);
    }

    @Override
    public void catchesSuccessfully(String articlename) {

    }

    @Override
    public void notCatched(String articlename) {

    }

    @Override
    public void releasedSuccessfully(String articlename) {
        Toast.makeText(this, "Article " + articlename + " Released " +
                articlename + "Successfully!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername(), MyCatchActivity.class);
    }

    @Override
    public void notReleased(String articlename) {
        Toast.makeText(this, " Couldn't Release " + articlename, Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername(), MyCatchActivity.class);
    }


}
