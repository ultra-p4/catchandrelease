package com.schue.stephan.catchandrelease.login;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface LoginActivityObserver {

    public void userIsAuthenticated(String username);

    public void userIsNotAuthenticated(String username);

    public void userDataNotReceived(String username);
}
