package com.schue.stephan.catchandrelease.release;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReleaseModel {

    private ReleaseObserver releaseObserver;
    private ReleaseDataProvider database = new ReleaseInJson();

    public ReleaseModel(ReleaseObserver releaseObserver) {
        this.releaseObserver = releaseObserver;
    }


/*    public ReleaseModel(MainActivityRelease mainActivityRelease) {

    }*/


    public void saveArticle(String username, String articlename, String articlecategory, String description, String price){

        boolean storeState =  database.storeArticle(username, articlename, articlecategory,description,price);

        if (storeState == true) {
            releaseObserver.storedArticle(articlename);
        } else {
            releaseObserver.notStoredArticle(articlename);
        }

    }


}
