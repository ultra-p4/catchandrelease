package com.schue.stephan.catchandrelease.login;

import android.app.Activity;

/**
 * Created by systemadmin on 16.07.2017.
 */

public interface LoginActivityProvider {

        void loginTriggerd(String username, String password);

        void goToProductActivity(Activity activityname, String username);
}
