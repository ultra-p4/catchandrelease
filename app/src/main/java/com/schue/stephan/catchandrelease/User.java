package com.schue.stephan.catchandrelease;

import android.util.JsonReader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class User {

    public String username;
    public String password;
    public String firstname;
    public String lastname;
    public String emailname;
    public String membersince;
    public String street;
    public String plz;
    public String city;

    //Konstruktoren

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public User () {}

    public User(String username) {
        this.username = username;
    }

    public User(String firstname, String lastname, String emailname, String membersince) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailname = emailname;
        this.membersince = membersince;
    }


    public User(String username, String password, String firstname, String lastname, String emailname,
                String membersince, String street, String plz, String city) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailname = emailname;
        this.membersince = membersince;
        this.street = street;
        this.plz = plz;
        this.city =city;
    }


    //Methoden

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmailname() {
        return emailname;
    }

    public void setEmailname(String emailname) {
        this.emailname = emailname;
    }

    public String getMembersince() {
        return membersince;
    }

    public void setMembersince(String membersince) {
        this.membersince = membersince;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }









    public class JsonStream {


        public User readUser(JsonReader reader) throws IOException {
            String username = null;
            String password = null;

            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("name")) {
                    username = reader.nextString();
                } else if (name.equals("password")) {
                    password = reader.nextString();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            return new User(username, password);
        }




    public String buildJsonObject(User user) {
        String jsonObjectOutput = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("username", user.getUsername());
            jsonObject.accumulate("password", user.getPassword());
            jsonObject.accumulate("firstname", user.getFirstname());
            jsonObject.accumulate("lastname", user.getLastname());
            jsonObject.accumulate("emailname", user.getEmailname());
            jsonObject.accumulate("membersince", user.getMembersince());
            jsonObject.accumulate("street", user.getStreet());
            jsonObject.accumulate("plz", user.getPlz());
            jsonObject.accumulate("city", user.getCity());
        } catch (JSONException e) {
            e.printStackTrace();
            System.err.println(e.toString());
        }
        jsonObjectOutput = jsonObject.toString();
        System.out.println(jsonObject.toString());
        return jsonObjectOutput;
    }



    public User readUserData(JsonReader reader) throws IOException {
        User user = new User();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("username")) {
                user.setUsername(reader.nextString());
            } else if (name.equals("password")) {
                user.setPassword(reader.nextString());
            } else if (name.equals("firstname")) {
                user.setFirstname(reader.nextString());
            } else if (name.equals("lastname")) {
                user.setLastname(reader.nextString());
            } else if (name.equals("emailname")) {
                user.setEmailname(reader.nextString());
            } else if (name.equals("membersince")) {
                user.setMembersince(reader.nextString());
            } else if (name.equals("street")) {
                user.setStreet(reader.nextString());
            } else if (name.equals("plz")) {
                user.setPlz(reader.nextString());
            } else if (name.equals("city")) {
                user.setCity(reader.nextString());
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return user;
    }




    }

}



