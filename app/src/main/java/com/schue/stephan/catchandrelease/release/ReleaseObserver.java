package com.schue.stephan.catchandrelease.release;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReleaseObserver {

    public void storedArticle(String articlename);

    public void notStoredArticle(String articlename);

}
