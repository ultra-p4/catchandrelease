package com.schue.stephan.catchandrelease.newkonto;

import com.schue.stephan.catchandrelease.User;

/**
 * Created by systemadmin on 30.07.2017.
 */

public interface KontoDataProvider {

    public boolean storeUser(User user);
}
