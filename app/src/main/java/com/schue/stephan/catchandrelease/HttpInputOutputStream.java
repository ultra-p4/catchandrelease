package com.schue.stephan.catchandrelease;

import android.os.StrictMode;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;






public class HttpInputOutputStream {

    private InputStream inputStream;
    private HttpURLConnection urlConnection;
    private boolean statusResponse;
    private URL url;




    public HttpInputOutputStream() { }




    public InputStream getInputStream(String link) throws IOException {
        setStrictPolicyMode();
        establishConnectionUrl(link);
        return inputStream;
    }





    public boolean setOutputStream(String link, String jsonObject) throws IOException {
        setStrictPolicyMode();
        establishJsonOutputStream(link, jsonObject);
        getResponseCode();
        return statusResponse;
    }





    private void setStrictPolicyMode () {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }






    private void establishConnectionUrl (String link) {
        try {
            URL url = new URL(ServerConnection.getPROTOCOL() + ServerConnection.getIP() +
                    ":" + ServerConnection.getPORT() + link);
            urlConnection = (HttpURLConnection) url.openConnection();
            inputStream = new BufferedInputStream(urlConnection.getInputStream());
            System.out.println("HTTP: Connection successfully established!");
        } catch (Exception e) {
            System.err.println("HTTP: Connection couldn't established!");
            e.printStackTrace();
        }
    }





    private void establishJsonOutputStream (String link, String jsonObject) {
        try {
            url = new URL(ServerConnection.getPROTOCOL() + ServerConnection.getIP() +
                    ":" + ServerConnection.getPORT() + link);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(0);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(jsonObject.getBytes());
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }





    private void getResponseCode () {
        try {
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == 201) {
                statusResponse = true;
                System.out.println(statusCode);
            }
            else {
                statusResponse = false;
                System.err.println(statusCode);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    public void closeConnection() {
        if (urlConnection != null) {
            urlConnection.disconnect();
            System.out.println("HTTP: Successfully disconnected!");
            return;
        }
        System.err.println("HTTP: Connection was null!");
    }




}
