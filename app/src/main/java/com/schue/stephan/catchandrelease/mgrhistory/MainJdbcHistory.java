package com.schue.stephan.catchandrelease.mgrhistory;

import com.schue.stephan.catchandrelease.Article;

import java.io.IOException;
import java.util.List;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface MainJdbcHistory {

    public List<Article> getArticles() throws IOException;

    public Article findArticle(String articlename);

}
