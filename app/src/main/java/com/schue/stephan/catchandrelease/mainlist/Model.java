package com.schue.stephan.catchandrelease.mainlist;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.Statistics;

import java.io.IOException;
import java.util.List;


public class Model {

    private Observer observer;
    private DataProvider database = new Json();
    private Article currentArticle;
    List<Article> articles = null;



    public Model(Observer observer) {
        this.observer = observer;
    }



    public List<Article> getArticle() throws IOException {
        return database.getArticles();
    }



    public Article getCurrentArticle() {
        return currentArticle;
    }




    public void setCurrentArticle(Article article) {
        currentArticle = article;
        observer.articleChanged();
    }




    public Article findArticle(String articlename) {
        return database.findArticle(articlename);
    }




    public void fetchArticles() {
        try {
            articles = database.getArticles();
        } catch (IOException e) {
            e.printStackTrace();
        }
        validateArticleList();
    }




    public void fetchMyCatchArticles(String username) {
        try {
            articles = database.getMyCatchArticles(username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        validateArticleList();
    }



    private void validateArticleList() {
        if (articles != null) {
            if (articles.size() != 0) {
                observer.listResultValid(articles);
                return;
            } else {
                observer.listResultEmpty();
            }
        }
        observer.listResultInvalid("Database Or Connection Error!");
    }



    public void getStatistics() {
            Statistics statistics = database.getStatistics();
        if (statistics != null) {
            observer.objectResultValid(statistics);
            return;
        }
        observer.objectResultInvalid("Connection Or Database Error!");
    }


    public void getHistory() {
        articles = database.getHistory();
        validateArticleList();
    }


}

