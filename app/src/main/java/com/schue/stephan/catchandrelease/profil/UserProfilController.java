package com.schue.stephan.catchandrelease.profil;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class UserProfilController implements UserProfilActivityProvider {

    private UserProfilModel model;

    UserProfilController(UserProfilModel model) {
        this.model = model;
    }


    @Override
    public void profilTriggered(String username) {
            model.getUserData(username);
    }
}
