package com.schue.stephan.catchandrelease.reverse;

import com.schue.stephan.catchandrelease.Article;

import java.util.List;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReverseModel {

    private ReverseObserver observer;
    private ReverseDataProvider database = new ReverseJson();

    private Article currentArticle;

    //Konstruktor
    public ReverseModel(ReverseObserver observer) {
        this.observer = observer;
    }

    public List<Article> getArticle(String username) {
        return database.getArticles(username);
    }

    public Article getCurrentArticle() {
        return currentArticle;
    }

    public void setCurrentArticle(Article article) {
        currentArticle = article;
        observer.articleChanged();
    }


    public Article findArticle(String articlename) {
        return database.findArticle(articlename);
    }


    public List<Article> fetchArticles(String username) {

        List<Article> Dbresult;
        Dbresult = database.getArticles(username);
        return Dbresult;

    }

}
