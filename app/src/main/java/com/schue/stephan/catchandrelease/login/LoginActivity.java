package com.schue.stephan.catchandrelease.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.newkonto.KontoMainActivity;



public class LoginActivity extends AppCompatActivity implements LoginActivityObserver {

    private LoginModel model = new LoginModel(this);
    private LoginActivityController controller = new LoginActivityController(model);



    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText usernameText = findViewById(R.id.usernameText);
        EditText passwordText = findViewById(R.id.passwordText);
        Button logOn = findViewById(R.id.button);
        Button registration = findViewById(R.id.buttonRegistration);


        logOn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //Überprüfung der Eingaben in den Textfeldern

                if(TextUtils.isEmpty(usernameText.getText().toString())) {
                    usernameText.setError("Attribut cannot be empty!");
                }
                else if(TextUtils.isEmpty(passwordText.getText().toString())) {
                    passwordText.setError("Attribut cannot be empty!");
                }
                else {
                    controller.loginTriggerd(usernameText.getText().toString(),
                            passwordText.getText().toString());
                }

            }
        });

        registration.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent registrationintent = new Intent (LoginActivity.this, KontoMainActivity.class);
                startActivity(registrationintent);
            }
        });

}


    @Override
    public void userIsAuthenticated (String username){
        Toast.makeText(this, " Here We Are " + username + " :)", Toast.LENGTH_LONG).show();
            controller.goToProductActivity(this, username);
    }



    @Override
    public void userIsNotAuthenticated (String username){
        Toast.makeText(this, "Invalid!", Toast.LENGTH_LONG).show();
    }



    @Override
    public void userDataNotReceived(String username) {
        Toast.makeText(this, "Couldn't Request Userdata By Server!", Toast.LENGTH_LONG).show();
    }


}
