package com.schue.stephan.catchandrelease.newkonto;

/**
 * Created by systemadmin on 30.07.2017.
 */

public interface KontoObserver {


    public void storedUser(String newUsername);

    public void notStoredUser(String newUsername);
}
