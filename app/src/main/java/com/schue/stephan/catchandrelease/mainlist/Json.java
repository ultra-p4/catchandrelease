package com.schue.stephan.catchandrelease.mainlist;

import android.util.JsonReader;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.HttpInputOutputStream;
import com.schue.stephan.catchandrelease.Statistics;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephan on 09.08.17.
 */

public class Json implements DataProvider {


    private static String INPUT_LINK = "/RestApp/rest/mainlist/fetch";
    private static String INPUT_LINK_FIND_MY_CATCHES = "/RestApp/rest/mainlist/mycatches?username=";
    private static String INPUT_LINK_STATISTICS = "/RestApp/rest/mainlist/statistics/get";
    private static String INPUT_LINK_HISTORY = "/RestApp/rest/mainlist/history/fetch";
    private List<Article> articles = new ArrayList<>();
    private Statistics statistics;
    private Object object;




    @Override
    public List<Article> getArticles() {
        try {
            readJsonStreamArticles(INPUT_LINK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return articles;
    }



    @Override
    public List<Article> getMyCatchArticles(String username) throws IOException {
        try {
            readJsonStreamArticles(INPUT_LINK_FIND_MY_CATCHES + URLEncoder.encode(username,"UTF-8"));
            return articles;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return articles;
    }



    @Override
    public Article findArticle(String articlename) {
        return null;
    }





    @Override
    public Statistics getStatistics() {
        try {
            readJsonStreamStatistics(INPUT_LINK_STATISTICS);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return statistics;
    }



    @Override
    public List<Article> getHistory() {
        try {
            readJsonStreamArticles(INPUT_LINK_HISTORY);
            return articles;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return articles;
    }




    private void readJsonStreamArticles(String link) throws IOException {
          HttpInputOutputStream httpInputOutputStream = new HttpInputOutputStream();
        JsonReader reader = new JsonReader(
                new InputStreamReader(
                        httpInputOutputStream.getInputStream(link) , "UTF-8"));
        try {
           Article.JsonStream jsonStream = (new Article()).new JsonStream();
         articles = jsonStream.ReadArticlesArray(reader);
        } finally {
            reader.close();
        }
        httpInputOutputStream.closeConnection();
    }





    private void readJsonStreamStatistics(String link) throws IOException {
        HttpInputOutputStream httpInputOutputStream = new HttpInputOutputStream();
        JsonReader reader = new JsonReader(
                new InputStreamReader(
                        httpInputOutputStream.getInputStream(link) , "UTF-8"));
        try {
            Statistics.JsonStream jsonStream = (new Statistics()).new JsonStream();
            statistics = jsonStream.readStatistics(reader);
        } finally {
            reader.close();
        }
        httpInputOutputStream.closeConnection();
    }




}
