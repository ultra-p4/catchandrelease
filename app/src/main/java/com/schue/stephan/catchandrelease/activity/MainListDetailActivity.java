package com.schue.stephan.catchandrelease.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;
import com.schue.stephan.catchandrelease.mainlistdetail.MainListDetailController;
import com.schue.stephan.catchandrelease.mainlistdetail.MainListDetailModel;
import com.schue.stephan.catchandrelease.mainlistdetail.MainListDetailObserver;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class MainListDetailActivity extends AppCompatActivity implements MainListDetailObserver {

    private MainListDetailModel articleModel = new MainListDetailModel(this);
    private MainListDetailController controller = new MainListDetailController(articleModel);
    TextView titelText;
    TextView beschreibungText;
    TextView categoryText;
    TextView releaserText;
    TextView priceText;
    Button catchButton;
    String articlename;
    User user = new User("user");


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mainlistdetail);


        titelText = (TextView) findViewById(R.id.titelText);
        beschreibungText = (TextView) findViewById(R.id.beschreibungText);
        categoryText = (TextView) findViewById(R.id.categoryText);
        releaserText = (TextView) findViewById(R.id.releaserText);
        priceText = (TextView) findViewById(R.id.preisText);
        catchButton = (Button) findViewById(R.id.catchArticle);

        user.setUsername(getIntent().getStringExtra("USER_NAME"));
        articlename = (getIntent().getStringExtra("ARTICLE_NAME"));

        controller.seeDetailTriggered(articlename);

        beschreibungText.setMovementMethod(new ScrollingMovementMethod());
        beschreibungText.setFocusable(true);
        beschreibungText.setFocusableInTouchMode(true);

        if (!user.getUsername().equals(releaserText.getText())) {
            catchButton.setClickable(true);
        } else {
            catchButton.setClickable(false);
            catchButton.setEnabled(false);
            catchButton.setBackgroundColor(0xFFFFFFFF);
        }


        catchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.catchArticleTriggered(user.getUsername(), articlename);
            }
        });


    }

    @Override
    public void productSelected(String articlename, String label, String description, String releaser, String articlecategory, String price) {
        titelText.setText(articlename);
        beschreibungText.setText(label);
        categoryText.setText(articlecategory);
        releaserText.setText(releaser);
        priceText.setText(description);
    }

    @Override
    public void productNotExisting(String articlename) {
        Toast.makeText(this, "Article " + articlename + " Not Exists!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername(), MainActivity.class);
    }

    @Override
    public void catchesSuccessfully(String articlename) {
        Toast.makeText(this, "Catched " + articlename + " Successfully :)", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername(), MainActivity.class);
    }

    @Override
    public void notCatched(String articlename) {
        Toast.makeText(this, "Couldn't Catch " + articlename + " :(", Toast.LENGTH_LONG).show();
    }

    @Override
    public void releasedSuccessfully(String articlename) {
        Toast.makeText(this, "Released " + articlename + " Successfully :)", Toast.LENGTH_LONG).show();
    }

    @Override
    public void notReleased(String articlename) {
        Toast.makeText(this, "Couldn't Release" + articlename + " :(", Toast.LENGTH_LONG).show();
    }


}
