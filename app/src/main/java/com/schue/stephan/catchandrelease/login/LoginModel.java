package com.schue.stephan.catchandrelease.login;

import com.schue.stephan.catchandrelease.User;

import java.io.IOException;



public class LoginModel {

    private LoginActivityObserver observer;
    private LoginDataProvider database = new LoginJson();


    LoginModel(LoginActivityObserver observer) {
        this.observer = observer;
    }




    public void authenticateUser(String username, String password) {
        User user = null;
        try {
          user =  database.findUser(username, password);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(user != null) {
            if (user.getUsername().equals(username)) {
                observer.userIsAuthenticated(username);
            } else {
                observer.userIsNotAuthenticated(username);
                //return;
            }
        }
        //observer.userDataNotReceived(username);
    }


}
