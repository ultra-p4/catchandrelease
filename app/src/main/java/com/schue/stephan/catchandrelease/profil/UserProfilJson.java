package com.schue.stephan.catchandrelease.profil;

import android.util.JsonReader;
import com.schue.stephan.catchandrelease.HttpInputOutputStream;
import com.schue.stephan.catchandrelease.User;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;



public class UserProfilJson implements UserProfilDataProvider {

    private User user;
    private static String INPUT_LINK = "/RestApp/rest/mainlist/user/profile?username=";



    @Override
    public User findUser(String username) throws IOException {
        try {
            readJsonStream(INPUT_LINK + URLEncoder.encode(username,"UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }




    public void readJsonStream(String link) throws IOException {
        HttpInputOutputStream httpInputOutputStream = new HttpInputOutputStream();
        JsonReader reader = new JsonReader(
                new InputStreamReader(
                        httpInputOutputStream.getInputStream(link) , "UTF-8"));
        try {
           User.JsonStream jsonStream = (new User()).new JsonStream();
            user = jsonStream.readUserData(reader);
        } finally {
            reader.close();
        }
        httpInputOutputStream.closeConnection();
    }


}
