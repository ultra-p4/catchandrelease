package com.schue.stephan.catchandrelease.mainlistdetail;

import android.app.Activity;
import android.content.Intent;



public class MainListDetailController implements MainListDetailProvider {

    private MainListDetailModel model;

    public MainListDetailController(MainListDetailModel model) {
        this.model = model;
    }


    @Override
    public void seeDetailTriggered(String articlename) {
        model.findArticle(articlename);
    }

    @Override
    public void catchArticleTriggered(String username, String articlename) {
        model.catchArticle(username, articlename);
    }

    @Override
    public void goToMainActivity(Activity pastActivity, String username, Class classFile) {
        Intent intent = new Intent(pastActivity, classFile).putExtra("USER_NAME", username);
        pastActivity.startActivity(intent);
    }

    @Override
    public void releaseCatchedArticle(String username, String articlename) {
            model.releaseCatchedArticle(username, articlename);
    }


}
