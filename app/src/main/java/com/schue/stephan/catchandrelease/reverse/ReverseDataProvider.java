package com.schue.stephan.catchandrelease.reverse;

import com.schue.stephan.catchandrelease.Article;

import java.util.List;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReverseDataProvider {

    public List<Article> getArticles(String username);

    public Article findArticle(String articlename);

}
