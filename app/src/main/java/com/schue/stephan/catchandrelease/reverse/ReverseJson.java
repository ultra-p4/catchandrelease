package com.schue.stephan.catchandrelease.reverse;

import android.os.StrictMode;
import android.util.JsonReader;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.ServerConnection;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephan on 12.08.17.
 */

public class ReverseJson implements ReverseDataProvider {

    private List<Article> articles = new ArrayList<>();
    private HttpURLConnection urlConnection;



    @Override
    public List<Article> getArticles(String username) {
        getInputStream(username);
        return articles;
    }

    @Override
    public Article findArticle(String articlename) {
        for (Article article:articles) {
            if (articlename.equals(article.getArticlename())) {
                return article;
            }
        }
        return null;
    }


    private void getInputStream(String username) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        InputStream in = null;

        try {
            URL url = new URL(ServerConnection.getPROTOCOL() + ServerConnection.getIP() +
                    ":" + ServerConnection.getPORT() + "/RestApp/rest/mainlist/reverse?username=" +
                    username);
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream());
            readJsonStream(in);
            System.out.println(in.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

    }



    private void readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            readArticlesArray(reader);
        } finally {
            reader.close();
        }

    }



    private void readArticlesArray(JsonReader reader) throws IOException {

        reader.beginArray();
        while (reader.hasNext()) {
            articles.add(readArticle(reader));
        }
        reader.endArray();
    }



    private Article readArticle(JsonReader reader) throws IOException {
        String articlename = null;
        String description = null;
        String price = null;
        String releaser = null;
        String rental_offer = null;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("articlename")) {
                articlename = reader.nextString();
            } else if (name.equals("description")) {
                description = reader.nextString();
            } else if (name.equals("price")) {
                price = reader.nextString();
            } else if (name.equals("releaser")) {
                releaser = reader.nextString();
            } else if (name.equals("articlecategory")) {
                rental_offer = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Article(articlename, description, price, releaser, rental_offer);
    }

}
