package com.schue.stephan.catchandrelease.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.schue.stephan.catchandrelease.BuildConfig;
import com.schue.stephan.catchandrelease.R;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class MainActivityAbout extends AppCompatActivity {

    private TextView versionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_about);

        versionText = (TextView) findViewById(R.id.versionTextView);

        //Version holen
        versionText.setText("APP Version " + BuildConfig.VERSION_NAME);

    }
}

