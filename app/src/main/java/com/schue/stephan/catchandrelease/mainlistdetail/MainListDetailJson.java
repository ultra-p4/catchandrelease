package com.schue.stephan.catchandrelease.mainlistdetail;

import android.util.JsonReader;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.HttpInputOutputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;

/**
 * Created by stephan on 12.08.17.
 */

public class MainListDetailJson implements MainListDetailInDataProvider {

    private Article article;
    private static String INPUT_LINK_FIND_ARTICLE = "/RestApp/rest/mainlist/search/main?articlename=";
    private static String INPUT_LINK_RENT_USER =  "/RestApp/rest/mainlist/rental?username=";
    private static String INPUT_LINK_ARTICLE = "&articlename=";
    private static String INPUT_LINK_UNDO_CATCH = "/RestApp/rest/mainlist/undocatch?username=";




    @Override
    public Article findArticle(String articlename) {
        //getInputStream(articlename);
        try {
            readJsonStream(INPUT_LINK_FIND_ARTICLE + URLEncoder.encode(articlename,"UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println(article.getArticlename());
        return article;
    }



    @Override
    public void rentalArticle(String username, String articlename) {
        try {
          readJsonStream(INPUT_LINK_RENT_USER +
                  URLEncoder.encode(username,"UTF-8") + INPUT_LINK_ARTICLE +
                  URLEncoder.encode(articlename,"UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void releaseCatchedArticle(String username, String articlename) {
        try {
            readJsonStream(INPUT_LINK_UNDO_CATCH +
                    URLEncoder.encode(username,"UTF-8") +
                    INPUT_LINK_ARTICLE + URLEncoder.encode(articlename,"UTF-8"));
        }    catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void readJsonStream(String link) throws IOException {
        HttpInputOutputStream httpInputOutputStream = new HttpInputOutputStream();
        JsonReader reader = new JsonReader(
                new InputStreamReader(
                        httpInputOutputStream.getInputStream (link)));
        try {
            Article.JsonStream jsonStream = (new Article()).new JsonStream();
            article = jsonStream.readArticle(reader);
        } finally {
            reader.close();
        }
        httpInputOutputStream.closeConnection();
    }


}