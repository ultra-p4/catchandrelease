package com.schue.stephan.catchandrelease.release;

import android.app.Activity;
import android.content.Intent;

import com.schue.stephan.catchandrelease.activity.MainActivity;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReleaseController implements ReleaseActivityProvider {

    private ReleaseModel model;

    public ReleaseController(ReleaseModel model) {
        this.model = model;
    }



    @Override
    public void releaseArticleTriggered(String username, String articlename, String articlecategory, String description, String price) {
        model.saveArticle(username, articlename, articlecategory, description, price);
    }

    @Override
    public void goToMainActivity(Activity pastActivity, String username) {

        Intent intent = new Intent(pastActivity, MainActivity.class).putExtra("USER_NAME", username);
        pastActivity.startActivity(intent);

    }

}
