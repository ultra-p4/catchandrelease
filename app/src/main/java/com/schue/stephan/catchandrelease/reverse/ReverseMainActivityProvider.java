package com.schue.stephan.catchandrelease.reverse;

import android.app.Activity;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReverseMainActivityProvider {

    public void articleListTriggered(String username);

    public void showDetails(Activity pastActivity, String username, String articlename);

    public void goToMainActivity(Activity pastActivity, String username);

}
