package com.schue.stephan.catchandrelease.reversedetail;

import android.os.StrictMode;
import android.util.JsonReader;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.ServerConnection;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by stephan on 12.08.17.
 */

public class ReverseDetailJson implements ReverseDetailDataProvider {

    private Article article;
    private HttpURLConnection urlConnection;
    private String encodedUrl;
    private boolean statusResponse;




    @Override
    public Article findArticle(String articlename) {
        getInputStream(articlename);
        System.out.println(article.getArticlename());
        return article;
    }



    @Override
    public String isReversePossible(String username, String articlename) {
        return null;
    }



    @Override
    public boolean rentalReverseArticle(String username, String articlename) {
        doReverse(username, articlename);
        return statusResponse;
    }







    //find Article


    private void getInputStream(String articlename) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        InputStream in = null;



        //Encodierung des Artikelnamens in URL Format

        try {
            encodedUrl = ServerConnection.getPROTOCOL() + ServerConnection.getIP() + ":" +
                    ServerConnection.getPORT() +
                    "/RestApp/rest/mainlist/reverse/detail?articlename=" +
                    URLEncoder.encode(articlename,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();


            //Verbinde...
        }
        try {
            URL url = new URL(encodedUrl);
            urlConnection = (HttpURLConnection) url.openConnection();

            in = new BufferedInputStream(urlConnection.getInputStream());
            readJsonStream(in);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

    }




    private void doReverse(String username, String articlename) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        InputStream in = null;



        //Encodierung des Artikelnamens in URL Format

        try {
            encodedUrl = "http://84.226.28.224:9080/RestApp/rest/mainlist/reverse/do?username=" +
                    URLEncoder.encode(username,"UTF-8") + "&articlename=" +
                    URLEncoder.encode(articlename,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();


            //Verbinde...
        }
        try {
            URL url = new URL(encodedUrl);
            urlConnection = (HttpURLConnection) url.openConnection();

         /*   in = new BufferedInputStream(urlConnection.getInputStream());
            readJsonStream(in);*/

            //Status Code
            int statusCode = urlConnection.getResponseCode();
            System.out.println(statusCode);
            System.out.println(statusCode);
            System.out.println(statusCode);

            if (statusCode == 201) {
                statusResponse = true;
            }
            else {
                statusResponse = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

    }








    //Sub Methoden


    private void readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            readArticle(reader);
        } finally {
            reader.close();
        }
    }



    private void readArticle(JsonReader reader) throws IOException {
        String articlename = null;
        String description = null;
        String price = null;
        String releaser = null;
        String rental_offer = null;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("articlename")) {
                articlename = reader.nextString();
            } else if (name.equals("description")) {
                description = reader.nextString();
            } else if (name.equals("price")) {
                price = reader.nextString();
            } else if (name.equals("releaser")) {
                releaser = reader.nextString();
            } else if (name.equals("articlecategory")) {
                rental_offer = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        article = new Article(articlename, description, price, releaser, rental_offer);
    }

}
