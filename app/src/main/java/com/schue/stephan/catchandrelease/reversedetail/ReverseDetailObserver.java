package com.schue.stephan.catchandrelease.reversedetail;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface ReverseDetailObserver {

    public void productSelected(String articlename, String label,
                                String description, String releaser, String articlecategory, String price);

    public void productNotExisting(String articlename);

    public void reversedSuccessfully(String articlename);

    public void notReversedSuccessfully(String articlename);

}
