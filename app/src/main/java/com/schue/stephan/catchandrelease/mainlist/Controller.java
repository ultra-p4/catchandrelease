package com.schue.stephan.catchandrelease.mainlist;

import android.app.Activity;
import android.content.Intent;

import com.schue.stephan.catchandrelease.Article;




public class Controller implements ControllerProvider {

    private Model model;



    public Controller(Model model) {
        this.model = model;
    }



    @Override
    public void articleListTriggered() {
       model.fetchArticles();
    }



    @Override
    public void showDetails(Activity pastActivity, String username, String articlename, Class classFile) {
        Article article = model.findArticle(articlename);
        Intent intent = new Intent(pastActivity, classFile);
                intent.putExtra("USER_NAME", username);
                intent.putExtra("ARTICLE_NAME", articlename);
        pastActivity.startActivity(intent);
        //model.setCurrentArticle(article);
    }



    @Override
    public void articleListMyCatchTriggered(String username) {
        model.fetchMyCatchArticles(username);
    }


    @Override
    public void managerTriggered() {
            model.getStatistics();
    }



    @Override
    public void historyListTriggered() { model.getHistory(); }


}
