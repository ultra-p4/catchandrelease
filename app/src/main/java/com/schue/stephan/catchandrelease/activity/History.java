package com.schue.stephan.catchandrelease.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.schue.stephan.catchandrelease.Article;
import com.schue.stephan.catchandrelease.mainlist.Controller;
import com.schue.stephan.catchandrelease.mainlist.Model;
import com.schue.stephan.catchandrelease.mainlist.Observer;
import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;
import com.schue.stephan.catchandrelease.login.LoginActivity;
import com.schue.stephan.catchandrelease.mainlist.listadapter.HistoryAdapter;
import com.schue.stephan.catchandrelease.profil.UserProfilActivity;
import com.schue.stephan.catchandrelease.release.MainActivityRelease;
import com.schue.stephan.catchandrelease.reverse.ReverseMainActivity;

import java.util.List;



public class History extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Observer {


    //private HistoryModel articleModel = new HistoryModel(this);
    //private HistoryController controller = new HistoryController(articleModel);
    User user = new User("user");
    TextView headerUsername;
    private Model articleModel = new Model(this);
    private Controller controller = new Controller(articleModel);



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user.setUsername(getIntent().getStringExtra("USER_NAME"));
        System.out.println(user.getUsername());
        setContentView(R.layout.activity_main);
        //Menubar zusammenstellen
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //Navigation zusammenstellen
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        controller.historyListTriggered();
    }






    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //Benutzername in Header einfügen
        headerUsername = (TextView) findViewById(R.id.navHeaderUserTextView);
        headerUsername.setText(user.getUsername());
        return true;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }





    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_profile) {
            Intent intent = new Intent(getBaseContext(), UserProfilActivity.class).putExtra("USER_NAME", user.getUsername());
            startActivity(intent);
        } else if (id == R.id.nav_home) {
            Intent intent = new Intent(getBaseContext(),MainActivity.class).putExtra("USER_NAME", user.getUsername());
            startActivity(intent);
        } else if (id == R.id.nav_torelease) {
            Intent intent = new Intent(getBaseContext(),MainActivityRelease.class).putExtra("USER_NAME", user.getUsername());;
            startActivity(intent);
        } else if (id == R.id.nav_mycatches) {
            Intent intent = new Intent(getBaseContext(),MyCatchActivity.class).putExtra("USER_NAME", user.getUsername());
            startActivity(intent);
        } else if (id == R.id.nav_reverse) {
            Intent intent = new Intent(getBaseContext(),ReverseMainActivity.class).putExtra("USER_NAME", user.getUsername());
            startActivity(intent);
        } else if (id == R.id.nav_manager) {
            Intent intent = new Intent(getBaseContext(),ManagerAcitvity.class).putExtra("USER_NAME", user.getUsername());
            startActivity(intent);
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(getBaseContext(),MainActivityAbout.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            Intent intent = new Intent(getBaseContext(),LoginActivity.class);
            //Back Stack History zurücksetzen
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    public void articleChanged() {
    }

    @Override
    public void listResultValid(List<Article> articles) {
        ListView articleListView = findViewById(R.id.articleListView);
        articleListView.setAdapter(new HistoryAdapter(user, this, controller,this, R.layout.content_main, articles));

    }

    @Override
    public void listResultInvalid(String errorMessage) {

    }

    @Override
    public void listResultEmpty() {

    }

    @Override
    public void objectResultValid(Object object) {

    }

    @Override
    public void objectResultInvalid(String errMessage) {

    }

    @Override
    public void objectResultEmpty() {

    }



}
