package com.schue.stephan.catchandrelease.login;

import com.schue.stephan.catchandrelease.User;

import java.io.IOException;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface LoginDataProvider {

        User findUser(String username, String password) throws IOException;

}
