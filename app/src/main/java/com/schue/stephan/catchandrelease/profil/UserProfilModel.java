package com.schue.stephan.catchandrelease.profil;

import com.schue.stephan.catchandrelease.User;
import java.io.IOException;



public class UserProfilModel {

    private UserProfilObserver observer;
    private UserProfilDataProvider database = new UserProfilJson();




    UserProfilModel(UserProfilObserver observer) {
        this.observer = observer;
    }




    void getUserData(String username) {
        User user = null;
        String errorMessage = "Database Error!";

        try {
            user =  database.findUser(username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(user != null) {
            if (user.getUsername().equals(username)) {
                observer.userDataProvided(user);
            } else {
                observer.userDataNotProvided(errorMessage);
            }
        }

    }


}
