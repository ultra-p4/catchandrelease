package com.schue.stephan.catchandrelease.profil;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;



public class UserProfilActivity extends AppCompatActivity implements UserProfilObserver {

    private EditText usernameText;
    private EditText firstnameText;
    private EditText lastnameText;
    private EditText membersinceText;
    private EditText emailText;
    private EditText addressText;
    User user;
    private UserProfilModel model = new UserProfilModel(this);
    private UserProfilController controller = new UserProfilController(model);





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        usernameText = (EditText) findViewById(R.id.editTextProfilUsername);
        firstnameText = (EditText) findViewById(R.id.editTextProfilFirstname);
        lastnameText = (EditText) findViewById(R.id.editTextProfilLastname);
        membersinceText = (EditText) findViewById(R.id.editTextProfilMember);
        emailText = (EditText)  findViewById(R.id.editTextProfilEmail);
        addressText = (EditText) findViewById(R.id.editTextProfilAddress);
        user = new User((getIntent().getStringExtra("USER_NAME")));
        try {
            controller.profilTriggered (user.getUsername());
    } catch   (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        usernameText.setFocusable(false);
        firstnameText.setFocusable(false);
        lastnameText.setFocusable(false);
        membersinceText.setFocusable(false);
        emailText.setFocusable(false);
        addressText.setFocusable(false);
    }




    @Override
    public void userDataProvided(User user) {
        usernameText.setText(user.getUsername());
        firstnameText.setText(user.getFirstname());
        lastnameText.setText(user.getLastname());
        membersinceText.setText(user.getMembersince());
        emailText.setText(user.getEmailname());
        addressText.setText(user.getStreet() + " " + user.getPlz() + " " + user.getCity());
    }




    @Override
    public void userDataNotProvided(String errorMessage) {
        usernameText.setText("");
        firstnameText.setText("");
        lastnameText.setText("");
        membersinceText.setText("");
        Toast.makeText(this, "You have no Account attributes!!!", Toast.LENGTH_LONG).show();
    }





}
