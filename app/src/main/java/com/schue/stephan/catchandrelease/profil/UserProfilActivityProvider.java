package com.schue.stephan.catchandrelease.profil;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface UserProfilActivityProvider {

    public void profilTriggered(String username);
}
