package com.schue.stephan.catchandrelease.mainlistdetail;



public interface MainListDetailObserver {

    void productSelected(String articlename, String label,
                                String description, String releaser, String articlecategory, String price);

    void productNotExisting(String articlename);

    void catchesSuccessfully(String articlename);

    void notCatched(String articlename);

    void releasedSuccessfully(String articlename);

    void notReleased(String articlename);

}
