package com.schue.stephan.catchandrelease.reversedetail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.schue.stephan.catchandrelease.R;
import com.schue.stephan.catchandrelease.User;

/**
 * Created by systemadmin on 10.06.2017.
 */

public class ReverseDetailMainActivity extends AppCompatActivity implements ReverseDetailObserver {

    ReverseDetailModel articleModel = new ReverseDetailModel(this);
    ReverseDetailController controller = new ReverseDetailController(articleModel);
    TextView titelText;
    TextView beschreibungText;
    TextView categoryText;
    TextView releaserText;
    TextView priceText;
    Button catchButton;
    User user = new User("user");
    String articlename;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mainlistdetail);


        titelText = (TextView) findViewById(R.id.titelText);
        beschreibungText = (TextView) findViewById(R.id.beschreibungText);
        categoryText = (TextView) findViewById(R.id.categoryText);
        releaserText = (TextView) findViewById(R.id.releaserText);
        priceText = (TextView) findViewById(R.id.preisText);
        catchButton = (Button) findViewById(R.id.catchArticle);

        user.setUsername(getIntent().getStringExtra("USER_NAME"));
        articlename = (getIntent().getStringExtra("ARTICLE_NAME"));

        controller.seeDetailTriggered(articlename);

        catchButton.setText("REVERSE !");
        catchButton.setBackgroundColor(0xFF4F5B66);  //Space-Gray ( RGB: 79, 91, 102)

        beschreibungText.setMovementMethod(new ScrollingMovementMethod());
        beschreibungText.setFocusable(true);
        beschreibungText.setFocusableInTouchMode(true);


        catchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.releaseArticleTriggered(user.getUsername(), articlename);
            }
        });


    }

    @Override
    public void productSelected(String articlename, String label, String description, String releaser, String articlecategory, String price) {

        titelText.setText(articlename);
        beschreibungText.setText(label);
        categoryText.setText(articlecategory);
        releaserText.setText(releaser);
        priceText.setText(description);
    }

    @Override
    public void productNotExisting(String articlename) {
        Toast.makeText(this, "Article " + articlename + " Not Exists!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername());
    }

    @Override
    public void reversedSuccessfully(String articlename) {
        Toast.makeText(this, "Article " + articlename + " reversed successfully!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername());
    }

    @Override
    public void notReversedSuccessfully(String articlename) {
        Toast.makeText(this, "Article " + articlename + " couldn't reverses (at this time)!", Toast.LENGTH_LONG).show();
        controller.goToMainActivity(this, user.getUsername());
    }


}
