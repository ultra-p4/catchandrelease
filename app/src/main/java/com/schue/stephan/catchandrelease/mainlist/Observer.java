package com.schue.stephan.catchandrelease.mainlist;

import com.schue.stephan.catchandrelease.Article;

import java.util.List;

/**
 * Created by systemadmin on 10.06.2017.
 */

public interface Observer {

    void articleChanged();

    void listResultValid(List<Article> articles);

    void listResultInvalid(String errorMessage);

    void listResultEmpty();

    void objectResultValid(Object object);

    void objectResultInvalid(String errMessage);

    void objectResultEmpty();



}
